* Fabrication DIY test masques
** Objectifs
** Matériels & Méthodes
- Casserole à pression
- Pot de confiture avec couvercle
- bouteilles en verre

*** Permettre à n'importe quelle personne confinée de tester la performance d'un masque 
*** Contraintes : 
- Matériel accessible (prix, local)

** Protocole
*** - Isoler une bactérie (si possible E. coli)
***  - Isoler des phages
***  - Construire une boîte à toux
**** Matériel
***  - Détection de phages qui auraient traversé le masque 
**** Matériel