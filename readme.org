Ce dépot regroupe les travaux de LaBase. Il compléte les inforamtions disponibles sur le wiki [[https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:covid19]]

** Balisage OrgMode

- le manuel complet d'orgmode : [[https://orgmode.org/orgguide.pdf]]
- Principaux élèments de syntaxe : [[http://karl-voit.at/2017/09/23/orgmode-as-markup-only/]]

** Bibliographie
 [[https://www.zotero.org/groups/2462558/bib]]